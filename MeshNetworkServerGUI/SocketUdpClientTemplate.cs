﻿using MeshNetworkServer;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeshNetworkServerGUI
{
    namespace MeshNetworkServerClient
    {
        using System.Collections.Generic;

        /*
         * ПЕРЕД НАЧАЛОМ РАБОТЫ ВАЖНО ПРОЧИТАТЬ КОД И ВСЕ КОММЕНТАРИИ В ЭТОМ ФАЙЛЕ.
         * Можно сделать свою реализацию UDP узла, а можно воспользоватьэтим шаблоном.
         * Шаблон специально коряво написан, чтобы у всех вышел разный код,
         * ведь все будут по разному решать проблемы и баги.
         * Для тестирования работы вашего клиента можно запустить приложение и стартануть и сервер,
         * сервер пишет в логи все свои действия, там читайте, через них дебажте
         */

        internal class SocketUdpClientTemplate
        {
            // Тут вы должны придумать как вы будете хранить все соседние узлы
            // Лучше, если ввод параметров соседних узлов будет из интерфейса или консоли
            // Здесь для примера храниться только 1 сосед
            private static string[] remoteAddress; // адрес для отправки

            private static readonly int remotePort = 8005; // порт для отправки
            private static readonly int localPort = 8004; // порт для получения

            private static readonly Task taskReceive;
            private static Task taskSend;

            private static CancellationTokenSource cancellationTokenSource;

            private static CancellationTokenSource tokenSource;

            private static CancellationToken tokenSend;

            private static Thread listenThread;

            private static UdpClient receiverClient;

            private static readonly List<Package> packages;

            private static uint[] idArray;

            public static void StartClient()
            {
                try
                {
                    idArray = new uint[255];
                    for (int i = 0; i < 255; i++)
                    {
                        idArray[i] = 0;
                    }
                    tokenSource = new CancellationTokenSource();
                    tokenSend = tokenSource.Token;
                    cancellationTokenSource = new CancellationTokenSource();
                    remoteAddress = Form1.DestinationIp;

                    // taskReceive = new Task(() => ReceiveMessage(tokenReceive));
                    listenThread = new Thread(ReceiveFunc)
                    {
                        Name = "receive packets from others"
                    };
                    listenThread.Start();
                    // taskReceive.Start();
                    taskSend = new Task(() => SendMessage(tokenSend));
                    taskSend.Start();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            private static void ReceiveFunc()
            {
                try
                {
                    receiverClient = new UdpClient(localPort)
                    {
                        EnableBroadcast = true
                    };

                    IPEndPoint remotEndPoint = null;
                    while (true)
                    {
                        byte[] dataBytes = receiverClient.Receive(ref remotEndPoint);

                        Package package = Package.FromBinary(dataBytes);
                        for (int i = 0; i < 255; i++)
                        {
                            if (idArray[i] == -1)
                            {
                                idArray[i] = package.PackageId;
                                packages[i] = package;
                                break;
                            }

                            if (idArray[i] == package.PackageId)
                            {
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            private static void SendMessage(CancellationToken token)
            {
                UdpClient sender = new UdpClient();
                byte[] data = new byte[Package.bufferSize];
                try
                {
                    while (true)
                    {
                        if (token.IsCancellationRequested)
                        {
                            return;
                        }
                        /* Здесь заполняете данные о ваших датчиках при помощи методов в файле Program.cs
                        * Для примерпа представлен вызов функции GenerateData, но это просто пример!
                        */
                        //   Package pack = new Package(MeshNetworkServerGUI.Form1.OurDataStructure);
                        // ushort node=
                        Package pack = new Package(MeshNetworkServerGUI.Form1.OurDataStructure);
                        pack.ToBinary(data);
                        for (int i = 0; i < remoteAddress.Length; i++)
                        {
                            sender.Send(data, data.Length, remoteAddress[i], remotePort);
                        }
                        Thread.Sleep(1000);//задержка между сообщениями
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
                finally
                {
                    sender.Close();
                }
            }

            private static Package GenerateData()
            {
                Package pack = new Package
                {
                    //Здесь вы генирируете пакеты с реалистичными рандомными значениями
                    //Не просто рандом, а хотябы в реалистичных границах
                    //Для понимания смотрите файл Package.cs
                    PackageId = 11,        // Для примера
                    NodeId = 1,            // Для примера
                    Time = DateTime.Now,   // Для примера
                    Humidity = 11,         // Для примера
                    IsFire = false,        // Для примера
                    Lighting = 11,         // Для примера
                    Pressure = 11,         // Для примера
                    Temperature = 11      // Для примера
                };
                //
                return pack;
            }

            //private static void ReceiveMessage(CancellationToken token)
            //{
            //    UdpClient receiver = new UdpClient(localPort);
            //    IPEndPoint remoteIp = null; // адрес входящего подключения
            //    try
            //    {
            //        while (true)
            //        {
            //            if (CancellationTokenSource.IsCancellationRequested)
            //            {
            //                receiver.Close();
            //                //  taskReceive.Dispose();
            //                return;
            //            }

            //            byte[] data = receiver.Receive(ref remoteIp); // входящий пакет байт
            //            Package pack = Package.FromBinary(data); //преобразование в пакет
            //                                                     /*
            //                                                      * Здесь нужно проверить id пакета (как? - смотри файл Package.cs и думай)
            //                                                      * И если пакет с таким id ранее не был получен, то:
            //                                                      *     - отправить всем соседям
            //                                                      *      (хорошо бы использовать широкофещательную рассылку: https://metanit.com/sharp/net/5.3.php)
            //                                                      *     - сохранить его id в список или перезаписываемый массив
            //                                                      *      (достаточно хранить 255 последних пакетов)
            //                                                      * Иначе забыть про этот пакет
            //                                                      */
            //        }
            //    }
            //    catch (Exception exception)
            //    {
            //        MessageBox.Show(exception.Message);
            //    }
            //    finally
            //    {
            //        receiver.Close();
            //    }
            //}

            public static void ClientStop()
            {
                // Эту функцию тоже желательно не так коряво реализовать,
                // она на данный момент вообще не всего клиента завершает
                // receiveThread.Abort();

                receiverClient.Close();
                cancellationTokenSource.Cancel();
                listenThread.Abort();
                tokenSource.Cancel();
                //  taskReceive.Wait();
                // listenThread.Abort();
            }
        }
    }
}