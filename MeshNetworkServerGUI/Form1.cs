﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeshNetworkServerGUI
{
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    public partial class Form1 : Form
    {
        private bool flag_server = false;
        private bool flag_client = false;

        //  public  uint testInt { get; set; }
        public struct DataStructure
        {
            public ushort NodeId { get; set; }
            public ushort Pressure { get; set; }

            public ushort Lighting { get; set; }

            public sbyte Temperature { get; set; }

            public ushort Humidity { get; set; }

            public bool fire;

            public DataStructure(ushort humidity, sbyte temperature, ushort lighting, ushort pressure, bool fire, ushort nodeId)
            {
                Humidity = humidity;
                Temperature = temperature;
                Lighting = lighting;
                Pressure = pressure;
                this.fire = fire;
                NodeId = nodeId;
            }
        }

        public static DataStructure OurDataStructure;

        //public static string DestinationIp = string.Empty;
        public static string[] DestinationIp;
        private int sleepTime = 1000;

        private bool getDataFlag;

        public Form1()
        {
            InitializeComponent();
            InitFunc();
        }

        private void InitFunc()
        {
            lb_local_ip.Text += GetLocalIPAddress();
        }

        private void ButtonStartClick(object sender, EventArgs e)
        {
            if (!flag_server)
            {
                Task ServerTask = new Task(StartServer);
                //   ServerTask.
                ServerTask.Start();
                button_start.BackColor = Color.Green;
                button_start.Text = "Stop server";
                flag_server = true;
            }
            else
            {
                MeshNetworkServerSocket.SocketUdpServer.SocketListenEnd();
                flag_server = false;
                button_start.BackColor = Color.Red;
                button_start.Text = "Start server";
            }
        }

        private void StartServer()
        {
            MeshNetworkServerSocket.SocketUdpServer.SocketListenStart(8005);
        }

        private void ButtonClientClick(object sender, EventArgs e)
        {
            Thread dataThread = new Thread(GetDataFunc)
            {
                Name = "Get data thread"
            };

            if (!flag_client)
            {
                DestinationIp = ip_addesses_tb.Lines;
                dataThread.Start();
                getDataFlag = true;
                Task ClientTask = new Task(MeshNetworkServerClient.SocketUdpClientTemplate.StartClient);
                ClientTask.Start();
                button_client.BackColor = Color.Green;
                button_client.Text = "Stop test client";
                flag_client = true;
            }
            else
            {
                dataThread.Abort();
                getDataFlag = false;
                MeshNetworkServerClient.SocketUdpClientTemplate.ClientStop();
                flag_client = false;
                button_client.BackColor = Color.Red;
                button_client.Text = "Start test client";
            }
        }

        private string GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ipAddress in host.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ipAddress.ToString();
                }
            }
            throw new Exception("No network adapters with an IP v4");
        }

        private void GetDataFunc()
        {
            while (getDataFlag)
            {
                ushort.TryParse(txtBox_node_number.Text, out ushort result);
                OurDataStructure.NodeId = result;

                ushort.TryParse(txtBox_Pressure.Text, out result);
                OurDataStructure.Pressure = result;

                ushort.TryParse(txtBox_humidity.Text, out result);
                OurDataStructure.Humidity = result;

                ushort.TryParse(txtBox_light.Text, out result);
                OurDataStructure.Lighting = result;

                sbyte.TryParse(txtBox_temperature.Text, out sbyte resultTemp);
                OurDataStructure.Temperature = resultTemp;

                OurDataStructure.fire = checkBox_fire.Checked;

                //   MeshNetworkServerGUI.Program.log.Info("Temperature= {0}", txtBox_temperature.Text);
                Thread.Sleep(sleepTime);
            }
            //throw new NotImplementedException();
        }
    }
}