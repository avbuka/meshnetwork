﻿namespace MeshNetworkServerGUI
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_start = new System.Windows.Forms.Button();
            this.button_client = new System.Windows.Forms.Button();
            this.lb_humidity = new System.Windows.Forms.Label();
            this.txtBox_humidity = new System.Windows.Forms.TextBox();
            this.lb_temperature = new System.Windows.Forms.Label();
            this.txtBox_temperature = new System.Windows.Forms.TextBox();
            this.lb_lighting = new System.Windows.Forms.Label();
            this.txtBox_light = new System.Windows.Forms.TextBox();
            this.lb_pressure = new System.Windows.Forms.Label();
            this.txtBox_Pressure = new System.Windows.Forms.TextBox();
            this.checkBox_fire = new System.Windows.Forms.CheckBox();
            this.lb_node_number = new System.Windows.Forms.Label();
            this.txtBox_node_number = new System.Windows.Forms.TextBox();
            this.lb_ip = new System.Windows.Forms.Label();
            this.lb_local_ip = new System.Windows.Forms.Label();
            this.ip_addesses_tb = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button_start
            // 
            this.button_start.BackColor = System.Drawing.Color.Red;
            this.button_start.Location = new System.Drawing.Point(54, 33);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(97, 36);
            this.button_start.TabIndex = 0;
            this.button_start.Text = "Start server";
            this.button_start.UseVisualStyleBackColor = false;
            this.button_start.Click += new System.EventHandler(this.ButtonStartClick);
            // 
            // button_client
            // 
            this.button_client.BackColor = System.Drawing.Color.Red;
            this.button_client.Location = new System.Drawing.Point(54, 91);
            this.button_client.Name = "button_client";
            this.button_client.Size = new System.Drawing.Size(97, 36);
            this.button_client.TabIndex = 1;
            this.button_client.Text = "Start test client";
            this.button_client.UseVisualStyleBackColor = false;
            this.button_client.Click += new System.EventHandler(this.ButtonClientClick);
            // 
            // lb_humidity
            // 
            this.lb_humidity.AutoSize = true;
            this.lb_humidity.Location = new System.Drawing.Point(51, 277);
            this.lb_humidity.Name = "lb_humidity";
            this.lb_humidity.Size = new System.Drawing.Size(47, 13);
            this.lb_humidity.TabIndex = 19;
            this.lb_humidity.Text = "Humidity";
            // 
            // txtBox_humidity
            // 
            this.txtBox_humidity.Location = new System.Drawing.Point(108, 274);
            this.txtBox_humidity.Name = "txtBox_humidity";
            this.txtBox_humidity.Size = new System.Drawing.Size(43, 20);
            this.txtBox_humidity.TabIndex = 18;
            this.txtBox_humidity.Text = "3";
            this.txtBox_humidity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_temperature
            // 
            this.lb_temperature.AutoSize = true;
            this.lb_temperature.Location = new System.Drawing.Point(51, 241);
            this.lb_temperature.Name = "lb_temperature";
            this.lb_temperature.Size = new System.Drawing.Size(46, 13);
            this.lb_temperature.TabIndex = 17;
            this.lb_temperature.Text = "Temper.";
            // 
            // txtBox_temperature
            // 
            this.txtBox_temperature.Location = new System.Drawing.Point(108, 238);
            this.txtBox_temperature.Name = "txtBox_temperature";
            this.txtBox_temperature.Size = new System.Drawing.Size(43, 20);
            this.txtBox_temperature.TabIndex = 16;
            this.txtBox_temperature.Text = "-61";
            this.txtBox_temperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_lighting
            // 
            this.lb_lighting.AutoSize = true;
            this.lb_lighting.Location = new System.Drawing.Point(51, 207);
            this.lb_lighting.Name = "lb_lighting";
            this.lb_lighting.Size = new System.Drawing.Size(44, 13);
            this.lb_lighting.TabIndex = 15;
            this.lb_lighting.Text = "Lighting";
            // 
            // txtBox_light
            // 
            this.txtBox_light.Location = new System.Drawing.Point(108, 204);
            this.txtBox_light.Name = "txtBox_light";
            this.txtBox_light.Size = new System.Drawing.Size(43, 20);
            this.txtBox_light.TabIndex = 14;
            this.txtBox_light.Text = "1421";
            this.txtBox_light.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_pressure
            // 
            this.lb_pressure.AutoSize = true;
            this.lb_pressure.Location = new System.Drawing.Point(51, 171);
            this.lb_pressure.Name = "lb_pressure";
            this.lb_pressure.Size = new System.Drawing.Size(48, 13);
            this.lb_pressure.TabIndex = 13;
            this.lb_pressure.Text = "Pressure";
            // 
            // txtBox_Pressure
            // 
            this.txtBox_Pressure.Location = new System.Drawing.Point(108, 168);
            this.txtBox_Pressure.Name = "txtBox_Pressure";
            this.txtBox_Pressure.Size = new System.Drawing.Size(43, 20);
            this.txtBox_Pressure.TabIndex = 12;
            this.txtBox_Pressure.Text = "760";
            this.txtBox_Pressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox_fire
            // 
            this.checkBox_fire.AutoSize = true;
            this.checkBox_fire.Location = new System.Drawing.Point(54, 318);
            this.checkBox_fire.Name = "checkBox_fire";
            this.checkBox_fire.Size = new System.Drawing.Size(97, 17);
            this.checkBox_fire.TabIndex = 20;
            this.checkBox_fire.Text = "Are we on fire?";
            this.checkBox_fire.UseVisualStyleBackColor = true;
            // 
            // lb_node_number
            // 
            this.lb_node_number.AutoSize = true;
            this.lb_node_number.Location = new System.Drawing.Point(194, 322);
            this.lb_node_number.Name = "lb_node_number";
            this.lb_node_number.Size = new System.Drawing.Size(68, 13);
            this.lb_node_number.TabIndex = 22;
            this.lb_node_number.Text = "nodeNumber";
            // 
            // txtBox_node_number
            // 
            this.txtBox_node_number.Location = new System.Drawing.Point(268, 318);
            this.txtBox_node_number.Name = "txtBox_node_number";
            this.txtBox_node_number.Size = new System.Drawing.Size(43, 20);
            this.txtBox_node_number.TabIndex = 21;
            this.txtBox_node_number.Text = "1";
            this.txtBox_node_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_ip
            // 
            this.lb_ip.AutoSize = true;
            this.lb_ip.Location = new System.Drawing.Point(194, 173);
            this.lb_ip.Name = "lb_ip";
            this.lb_ip.Size = new System.Drawing.Size(23, 13);
            this.lb_ip.TabIndex = 24;
            this.lb_ip.Text = "IP :";
            // 
            // lb_local_ip
            // 
            this.lb_local_ip.AutoSize = true;
            this.lb_local_ip.Location = new System.Drawing.Point(230, 45);
            this.lb_local_ip.Name = "lb_local_ip";
            this.lb_local_ip.Size = new System.Drawing.Size(62, 13);
            this.lb_local_ip.TabIndex = 25;
            this.lb_local_ip.Text = "My local IP:";
            // 
            // ip_addesses_tb
            // 
            this.ip_addesses_tb.Location = new System.Drawing.Point(223, 173);
            this.ip_addesses_tb.Name = "ip_addesses_tb";
            this.ip_addesses_tb.Size = new System.Drawing.Size(177, 139);
            this.ip_addesses_tb.TabIndex = 26;
            this.ip_addesses_tb.Text = "192.168.1.179\n192.168.1.183\n192.168.1.207\n192.168.1.154";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ip_addesses_tb);
            this.Controls.Add(this.lb_local_ip);
            this.Controls.Add(this.lb_ip);
            this.Controls.Add(this.lb_node_number);
            this.Controls.Add(this.txtBox_node_number);
            this.Controls.Add(this.checkBox_fire);
            this.Controls.Add(this.lb_humidity);
            this.Controls.Add(this.txtBox_humidity);
            this.Controls.Add(this.lb_temperature);
            this.Controls.Add(this.txtBox_temperature);
            this.Controls.Add(this.lb_lighting);
            this.Controls.Add(this.txtBox_light);
            this.Controls.Add(this.lb_pressure);
            this.Controls.Add(this.txtBox_Pressure);
            this.Controls.Add(this.button_client);
            this.Controls.Add(this.button_start);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_client;
        private System.Windows.Forms.Label lb_humidity;
        private System.Windows.Forms.TextBox txtBox_humidity;
        private System.Windows.Forms.Label lb_temperature;
        private System.Windows.Forms.TextBox txtBox_temperature;
        private System.Windows.Forms.Label lb_lighting;
        private System.Windows.Forms.TextBox txtBox_light;
        private System.Windows.Forms.Label lb_pressure;
        private System.Windows.Forms.TextBox txtBox_Pressure;
        private System.Windows.Forms.CheckBox checkBox_fire;
        private System.Windows.Forms.Label lb_node_number;
        private System.Windows.Forms.TextBox txtBox_node_number;
        private System.Windows.Forms.Label lb_ip;
        private System.Windows.Forms.Label lb_local_ip;
        private System.Windows.Forms.RichTextBox ip_addesses_tb;
    }
}

